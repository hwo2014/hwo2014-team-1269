import sys
import socket
from copy import deepcopy

from car import Car
from track import Track
from graph_magic import generate_graph, RACE_LINE
from djikstra import shortest_path
from base_bot import BaseBot
from util import debug_print, car_id

from situations import *


class WinningBot(BaseBot):

    def __init__(self, socket, name, key):
        super(WinningBot, self).__init__(socket, name, key)

        self.track_graph = None
        self.next_switch = None
        self.next_switch_decision = None
        # Once physics is known, then dist_to_switch_thresh = ceil(Vmax/60) 
        self.dist_to_switch_thresh = 20 # Loose constraint currently. 

        self.current_throttle = 0.6
        self.default_throttle = 0.6

        self.situations = [
            SituationPhysics(1000),
            SituationDefault(1),
        ]

        self.constants = None

    # Assumes that the lane is indexed from 0 to 1.. and there is no distance mismatch.
    def switch(self):
        """Call this when we want to make the switch"""
        if self.next_switch_decision == 'Left':
            self.switch_lane_left()
            self.next_switch_decision = None
        elif self.next_switch_decision == 'Right':
            self.switch_lane_right()
            self.next_switch_decision = None
        else:
            pass


    # TODO the + - 1 for right and left mapping need to be confirmed from the track direction and lane ordering 
    # potential bug!!!        
    def decode_switch_cmd(self, decision):
        lane = self.our_car.current_lane
        if decision == 'Left':
            return lane - 1
        elif decision == 'Right':
            return lane + 1
        else:
            return lane

    def refresh_switch_decision(self):
        """Call this on every cycle."""
        current_piece = self.our_car.current_piece
        current_lane = self.our_car.current_lane
        distance_to_next = self.our_car.current_in_piece_position - self.track.edge_length(current_piece,current_lane,current_lane)
        first_switch = self.track.next_switch(current_piece)
        second_switch = self.track.next_switch(first_switch + 1)

        if current_piece == self.next_switch and distance_to_next < self.dist_to_switch_thresh or self.next_switch is None:
            # set switch decision if no deadlock in calling method
            if self.deadlock(current_piece,current_lane):
                debug_print("deadlock found")
                edge_weights = self.get_edge_weights(first_switch + 1, second_switch)
                self.next_switch_decision = self.make_switch_decision(current_piece, current_lane, first_switch, edge_weights)
            self.next_switch = self.track.next_switch(current_piece)
            self.switch()

    def deadlock(self, current_piece, current_lane):
        """
        As soon as the threshold to the next switch has been reached this method is called.
        The preferred switch path is checked to see if any cars are detected.
        """
        first_switch = self.track.next_switch(current_piece)
        second_switch = self.track.next_switch(first_switch + 1)
        unity_weights = [None] # First solve shortest path without weights
        ideal_next_lane = self.make_switch_decision(current_piece, current_lane, first_switch, unity_weights)
        debug_print(("make_switch_decision returned: next ideal lane = ", ideal_next_lane))
        car_block = self.find_closet_car(self.decode_switch_cmd(ideal_next_lane), first_switch + 1, second_switch)
        if car_block == None:
            self.next_switch_decision = ideal_next_lane
            return False # no deadlock on desired path
        else:
            return True

    def find_closet_car(self, start_lane, start_piece, end_piece):
        """Returns the closest car on a given segments between switches""" 
        traffic_jam_list = []

        for name in self.cars:
            car = self.cars[name]
            if name != self.our_car_name:
                # edge is a tuple of (piece_num, start_lane, end_lane)
                opponent_piece = car.current_edge[0]
                opponent_lane  = car.current_edge[1]
                if  opponent_piece >= start_piece and opponent_piece <= end_piece and opponent_lane == start_lane:
                    traffic_jam_list.append(name)
            
        if traffic_jam_list == []:
            return None
        else:
            closest_car  = None
            min_distance = 1000000000
            # find closest car traffic list
            for name in traffic_jam_list:
                car_ahead_dist = self.cars[name].current_distance
                if car_ahead_dist < min_distance:
                    closest_car = name
                    min_distance = car_ahead_dist
            return self.cars[closest_car]

    def est_time_arrival(self, start_lane, start_piece, end_piece, car_block):
        """
        Requires that the path is a segment between switches and assumes contant speed for cars of interest.
        If time is available then we should update this method to to account for expected velocity of cars 
        at various positions through the track.
        Assumes our car starts at the switch 
        """
        s_us_history = self.our_car.speed
        s_us = sum(s_us_history)/len(s_us_history)

        # if in multi mode, for demonstration purpose, skew weightings to change lanes to avoid cars  
        #if self.globals.multi_race:
        #    s_us *= s_us * s_us # cubed speed to weigh towards changing lanes
        dist_total = self.segment_length(start_lane, start_piece, end_piece)
        travel_time = dist_total / s_us
   
        # Check to see if path ahead is effectively clear
        if car_block == None:
            debug_print(("Travel time w/o block: ", travel_time, " Lane: ", start_lane," Start piece: ", start_piece, " End Piece: ", end_piece))
            return travel_time
        s_them_history = car_block.speed
        s_them = sum(s_them_history)/len(s_them_history) 
        # Check to see if path ahead is effectively clear
        if s_them > s_us:
            debug_print(("Travel time w/o block: ", travel_time, " Lane: ", start_lane," Start piece: ", start_piece, " End Piece: ", end_piece))
            return travel_time

        else:
            dist_to_them = self.segment_length(start_lane, start_piece, car_block.current_piece) + car_block.current_in_piece_position
            dist_them_remain = dist_total - dist_to_them
            intersection_time = dist_to_them / (s_us - s_them)
            if (s_them * intersection_time) >= dist_them_remain:
                # travelling faster but wont catch them by the end of the segment
                debug_print(("Travel time wont catch up: ", travel_time, " Lane: ", start_lane," Start piece: ", start_piece, " End Piece: ", end_piece))
                return travel_time 
            else:
                # we will catch up with them and then max at their speed
                travel_time = intersection_time + (dist_them_remain - intersection_time * s_them) / s_them
                debug_print(("Travel time IMPASSE! : ", travel_time, " Lane: ", start_lane," Start piece: ", start_piece, " End Piece: ", end_piece))
                return travel_time


    def segment_length(self, start_lane, start_piece, end_piece):
        segment_length = 0
        if end_piece <= start_piece:
            return segment_length
        else:
            for p in range(start_piece, end_piece + 1):
                segment_length += self.track.edge_length(p, start_lane, start_lane)
            return segment_length

    def get_edge_weights(self, start_piece, end_piece):
        edge_weights = []
        debug_print(("Current Piece Edge (p,sl,el): ", self.our_car.current_edge ))
        for l in range(self.track.num_lanes()):
            car_block = self.find_closet_car(l,start_piece, end_piece)
            edge_weights.append(self.est_time_arrival(l, start_piece, end_piece, car_block))
        debug_print(edge_weights)
        return edge_weights


    def make_switch_decision(self, current_piece, current_lane, switch_piece, next_segment_weights):
        """
        Given current piece/lane and a particular switch, what should the
        optimal switch be.

        Note you need the next switch piece so you can give the decision as to
        what to do there (without it you know the edges, but you don't know the
        switch decisions.
        """

        # handles special case when switching laps
        if current_piece <= switch_piece: 
            start_vertice = (current_piece, current_lane)
        else:
            start_vertice = (0, current_lane)

        end_vertice = RACE_LINE
        new_graph = self.track_graph

        # update graph edges based on traffic jams 
        if next_segment_weights[0] != None:
            debug_print("Updating graph weights (main.py: line 163")
            new_graph = deepcopy(self.track_graph)
            first_switch = self.track.next_switch(current_piece)
            second_switch = self.track.next_switch(first_switch + 1)
            start_piece = first_switch + 1
            end_piece = second_switch
            update_range = None
            
            # handles special case if range falls between the finish line 
            if start_piece <= end_piece:
                update_range = range(start_piece,end_piece)
            else:
                update_range = range(start_piece, self.track.num_lanes()+1) + range(0, end_piece)

            for p in update_range:
                for l in range(self.track.num_lanes()):
                    vertices = new_graph[(p,l)].keys()
                    for v in vertices:
                        # update the edge weights with a weighted distance for each piece in the segments between switches
                        speed_hist = self.our_car.speed 
                        average_speed = sum(speed_hist) / len(speed_hist)
                        debug_print(("Before Adjustment...Edge: ",(p,l,v), "Old Weight: ", new_graph[(p,l)][v]))
                        new_graph[(p,l)][v] = (next_segment_weights[l] * average_speed) / len(update_range)
                        debug_print(("After  Adjustment...Edge: ",(p,l,v), "New Weight: ", new_graph[(p,l)][v]))

        path = shortest_path(new_graph, start_vertice, end_vertice)

        for i, vertice in enumerate(path):
            piece, lane = vertice
            if piece == switch_piece:
                next_lane = path[i + 1][1]
                if current_lane > next_lane:
                    decision = 'Left'
                    break
                elif current_lane < next_lane:
                    decision = 'Right'
                    break
                else:
                    decision = None
                    break
        return decision

   
    def decide(self):
        bot = self
        track = self.track
        cars = self.cars
        our_car = self.our_car

        active_situations = [s for s in self.situations if s.is_active(bot, track, cars, our_car)]
        active_situations.sort(key=lambda x: x.priority)

        for situation in active_situations:
            decision = situation.handle(bot, track, cars, our_car)
            if type(decision) == float:
                self.throttle(decision)
                break
            elif decision == "Left":
                self.switch_lane_left()
                break
            elif decision == "Right":
                self.switch_lane_right()
                break
            elif decision == "Boost":
                self.turbo()
                break
            else:
                continue

        self.print_debug_info()


    def print_debug_info(self):
        our_car = self.our_car
        current_piece = self.track.pieces[our_car.current_piece]
        in_piece_position = our_car.current_in_piece_position
        on_curve = 'angle' in current_piece

        radius = 0
        if on_curve:
            radius = current_piece['radius']
            if current_piece['angle'] < 0:
                radius = -radius

        speed = our_car.speed()

        if radius == 0:
            centripital_acceleration = 0
        else:
            centripital_acceleration = speed**2/radius

        if len(self.throttles) > 0:
            throttle = self.throttles[-1]
        else:
            throttle = 0

        print('{:10}, {:12.6f}, {:10.6f}, {:10.6f}, {:+10.6f}, {:+12.6f}, {:+12.6f}'.format(
            self.game_tick,
            our_car.current_distance,
            our_car.speed(),
            throttle,
            our_car.current_tilt,
            radius,
            centripital_acceleration
            ))


    def init_track(self, track_data):
        self.track = Track(track_data)
        self.track_graph = generate_graph(self.track)

    def init_cars(self, car_data):
        for d in car_data:
            name = car_id(d['id'])
            width = d['dimensions']['width']
            length = d['dimensions']['length']
            flag_position = d['dimensions']['guideFlagPosition']

            car = Car(name, width, length, flag_position, self.track)
            self.cars[name] = car

        self.our_car = self.cars[self.our_car_name]
        self.our_car.default_throttle = self.default_throttle



if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./ruzn host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        debug_print("Connecting with parameters:")
        debug_print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))

        bot = WinningBot(s, name, key)
        bot.run()

