DEBUG = True 
NUM_TEST_CARS = 3

class DebugGlobals:
	def __init__(self, multi_race, num_debug_cars, track_name, debug_password):
 		self.multi_race = multi_race
 		self.num_debug_cars = num_debug_cars
 		self.track_name = track_name
 		self.debug_password = debug_password
 		
def debug_print(string):
    if DEBUG:
        print(string)

def car_id(car):
    return car['name'] + '_'+ car['color']