
class Car(object):

    def __init__(self, name, width, length, flag_position, track):
        self.name = name
        self.width = width
        self.length = length
        self.flag_position = flag_position
        self.track = track

        # is turbo on for this car right now?
        self.turbo = False

        self.disqualified = False

        self.current_lane = None

        # total distance traveled
        self.current_distance = 0
        self.distances = []

        # total distance traveled, excluding current piece
        self._distance_up_to_current_piece = 0

        self.current_speed = None
        self.current_in_piece_position = None
        self.current_piece = None
        self.current_tilt = None

        # and edge is a tuple of (piece_num, start_lane, end_lane)
        self.current_edge = None

        self.switches = []

        self.crashed = False
        self.crashes = []

    def record_crash(self):
        self.crashed = True
        crash_details = {
            'position': self.current_distance,
            'lane': self.current_lane,
            'piece': self.current_piece,
        }
        self.crashes.append(crash_details)

    def update_distance(self, in_piece_position, piece_num, start_lane, end_lane):
        self.current_piece = piece_num
        self.current_in_piece_position = in_piece_position

        prev_edge = self.current_edge
        edge = (piece_num, start_lane, end_lane)
        self.current_edge = edge

        on_new_piece = prev_edge != edge

        if on_new_piece and not prev_edge is None:
            self._distance_up_to_current_piece += self.track.edge_length(*prev_edge)

        distance = self._distance_up_to_current_piece + in_piece_position
        self.current_distance = distance
        self.distances.append(distance)

    def speed(self):
        if len(self.distances) < 2:
            return 0
        else:
            return self.distances[-2] - self.distances[-1]

    def update_tilt(self, tilt):
        self.current_tilt = tilt

    def update_lane(self, lane, piece):
        switch_details = {
            'start': self.current_lane,
            'end': lane,
            'piece': piece
        }
        self.switches.append(switch_details)
        self.current_lane = lane

    def distance_to_next_curve(self):
        """Return distance next to next curve and the curve piece number."""
        piece = self.current_piece
        if 'radius' in piece:
            distance_left_to_curve
        else:
            pieces = self.track.pieces
            num_pieces = len(pieces)
            lane = self.current_lane
            current_piece_length = pieces[piece]['length']
            distance_left_in_piece = current_piece_length - self.current_in_piece_position

            distance_left_to_curve = distance_left_in_piece
            while not 'radius' in pieces[piece]:
                piece = (piece + 1) % num_pieces
                distance_left_to_curve += pieces[piece]['length']

        return distance_left_to_curve, piece

