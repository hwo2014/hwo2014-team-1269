from djikstra import shortest_path

RACE_LINE = 'FINISH LINE'

def generate_graph(track):
    """
    Takes number of lanes and a list of bools indicating whether it is a switch
    or not.
    """

    graph = {}
    num_lanes = track.num_lanes()
    num_pieces = track.num_pieces()

    for p, piece_object in zip(range(num_pieces), track.pieces):
        if 'switch' in piece_object:
            for l in range(num_lanes):
                vertices = {}

                if l == 0: 
                    vertices[(p + 1, l + 1)] = track.edge_length(p, l, l + 1)
                elif l == num_lanes - 1:
                    vertices[(p + 1, l - 1)] = track.edge_length(p, l, l - 1)
                else:
                    vertices[(p + 1, l + 1)] = track.edge_length(p, l, l + 1)
                    vertices[(p + 1, l - 1)] = track.edge_length(p, l, l - 1)
                vertices[(p + 1, l)] = track.edge_length(p, l, l)

                graph[(p, l)] = vertices

        else:
            for l in range(num_lanes):
                vertices = {}
                vertices[(p + 1, l)] = track.edge_length(p, l, l)
                graph[(p, l)] = vertices

    # add final node to account for the fact that we don't care which lane we
    # are in at the end of the race
    for l in range(num_lanes):
        graph[(p + 1, l)] = {RACE_LINE: 0}
    graph[RACE_LINE] = {}

    return graph


if __name__ == '__main__':
    import pickle
    import track
    import time
    
    f = open('finland_track_data.pkl', 'rb')
    raw_track_data = pickle.load(f)
    track = track.Track(raw_track_data)

    start = time.time()
    graph = generate_graph(track)
    stop = time.time()

    print(repr(graph))
    print(stop - start)


