import sys, socket

from main import WinningBot
from util import debug_print


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        debug_print("Connecting with parameters:")
        debug_print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = WinningBot(s, name, key)

        #track_name = 'germany'
        track_name = 'usa'
        #track_name = 'keimola'

        car_count = 3

        bot.run_test(track_name, car_count)

