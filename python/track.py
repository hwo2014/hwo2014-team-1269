import math


class Track(object):

    def __init__(self, track_data):
        # convert raw lane data into a dict with the lane number as the key,
        # and the offset as the value, as there is nothing to know about a lane
        # except what its offset it is (and that it exists)
        self.lanes = dict([(lane['index'], lane['distanceFromCenter']) for lane in track_data['lanes']])
    
        self.pieces = track_data['pieces']

        self._memoized_edge_lengths = {}

    def num_pieces(self):
        return len(self.pieces)

    def num_lanes(self):
        return len(self.lanes)

    def next_switch(self, start_piece):
        """Given a piece, determine when the next switch piece occurs."""
        num_pieces = self.num_pieces()
        piece = (start_piece + 1) % num_pieces
        while piece != start_piece:
            if 'switch' in self.pieces[piece]:
                return piece
            piece = (piece + 1) % num_pieces
        raise Exception()

    def edge_length(self, piece_num, start_lane, end_lane):
        """
        Calculate edge length for given piece and lanes indices (account for
        switching in the piece by providing start and end lanes).  Note that
        the lane indices must be valid for the track.

        Five possibilities:
            1. invalid switch (returns infinity)
            2. curved
            3. curved with switching
            4. straight
            5. straight with switching

        This function returns infinite length if the start and end lane are
        different on a piece that isn't a switch.
        """

        key = (piece_num, start_lane, end_lane)
        if key in self._memoized_edge_lengths:
            return self._memoized_edge_lengths[key]

        piece = self.pieces[piece_num]
        switching = start_lane != end_lane
        start_lane_offset = self.lanes[start_lane]
        end_lane_offset = self.lanes[end_lane]

        if switching and not 'switch' in piece:

            # 1. INVALID SWITCH
            edge_length = float('inf')

        elif 'radius' in piece:
            angle = abs(piece['angle'])*math.pi/180.0  # raw angle may be negative
            right_turn = piece['angle'] > 0
            center_line_radius = piece['radius']  # radius along the center of the track

            # 2. CURVED
            if not switching:
                if right_turn:
                    radius_offset = -start_lane_offset
                else:
                    radius_offset = start_lane_offset
                radius = center_line_radius + radius_offset
                edge_length = angle*radius

            # 3. CURVED AND SWITCHING
            else:
                # assume switched curves are always less than 90 degress

                if right_turn:
                    radius_offset = -end_lane_offset
                else:
                    radius_offset = end_lane_offset
                new_lane_radius = center_line_radius + radius_offset

                dx_offset = end_lane_offset - start_lane_offset
                if right_turn:
                    dx_curve = new_lane_radius*(1 - math.cos(angle))
                else:
                    dx_curve = new_lane_radius*(math.cos(angle) - 1)

                x = dx_offset + dx_curve
                y = new_lane_radius*math.sin(angle)

                fudge = 0.6  # heuristically determined
                edge_length = math.sqrt(x**2 + y**2)

        else:

            # 4. STRAIGHT
            if not switching:
                edge_length = piece['length']

            # 5. STRAIGHT AND SWITCHING
            else:
                # approximate as a triangle for now
                dy = abs(start_lane_offset - end_lane_offset)
                dx = piece['length']
                fudge = 0.079885  # heuristically determined
                edge_length = math.sqrt(dx**2 + dy**2) + fudge

        self._memoized_edge_lengths[key] = edge_length

        return edge_length

