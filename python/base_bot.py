import json
import random

from util import debug_print, car_id, DebugGlobals

class BaseBot(object):

    def __init__(self, socket, name, key):

        self.governor = None # only used in testing

        self.socket = socket
        self.name = name
        self.key = key
        
        self.game_tick = 0
        self.track = None
        self.cars = {}
        self.our_car = None
        self.our_car_name = None

        # are we in a qualifying round?
        self.qualify_round = None

        self.throttles = []

        self.reset_turbo()

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def throttle(self, throttle):
        self.msg("throttle", throttle)
        self.throttles.append(throttle)

    def switch_lane_left(self):
        self.msg('switchLane', "Left")

    def switch_lane_right(self):
        self.msg('switchLane', "Right")

    def turbo(self):
        self.reset_turbo()
        self.msg('turbo', 'vroooooooomm')

    def ping(self):
        self.msg("ping", {})

    def join(self):
        self.msg("join", {"name": self.name, "key": self.key })

    def run(self):
        self.join()
        self.msg_loop()

    def run_test(self, track_name, car_count, password='edge_effect'):
        self.name = "hell" + str(random.randint(1, 1000))
        print(self.name)
        data = {
            "botId": {
                "name": self.name,
                "key": self.key,
            },
            "password": password,
            "carCount": car_count,
            "trackName": track_name
        }
        
        self.governor = random.uniform(0.4, 0.7)

        self.msg("joinRace", data)
        self.msg_loop()

    def on_join(self, data):
        debug_print("Joined")
        self.ping()

    def on_game_start(self, data):
        debug_print("Race started")
        self.ping()

    def on_game_init(self, data):

        # game init is called twice; the first time is for the qualify round,
        # the second time is for the real race; set a boolean on the bot object
        # indicating which round it is in
        if self.qualify_round is None:
            self.qualify_round = True
        elif self.qualify_round:
            self.qualify_round = False

        debug_print("Initializing track")
        self.init_track(data['race']['track'])

        debug_print("Initializing cars")
        self.init_cars(data['race']['cars'])

    def on_car_positions(self, car_data):
        self.update_car_positions(car_data)
        self.decide()

    def decide(self):
        raise NotImplementedError()

    def on_lap_finished(self, data):
        debug_print("Lap Finished")
        self.ping()

    def on_crash(self, data):
        debug_print("Someone crashed")
        name = car_id(data)
        car = self.cars[name]
        car.record_crash()
        self.ping()

    def on_spawn(self, data):
        debug_print("Spawning")

        # update the car indicating it is no longer crashed
        name = car_id(data)
        self.cars[name].crashed = False

        self.ping()

    def on_finish(self, data):
        debug_print("Finished the race")
        self.ping()

    def on_game_end(self, data):
        debug_print("Race ended")
        self.ping()

    def on_tournament_end(self, data):
        debug_print("Tournament ended")
        self.ping()

    def on_error(self, data):
        debug_print("Error: {0}".format(data))
        self.ping()

    def on_your_car(self, data):
        self.our_car_name = car_id(data)
        self.ping()

    def on_disqualified(self, data):
        name = car_id(data['car'])
        car = self.cars[name]
        car.disqualified = True

        print("DISQUALIFIED: {}".format(data['reason']))
        self.ping()

    def on_turbo_start(self, data):
        name = car_id(data)
        car = self.cars[name]
        car.turbo = True

    def on_turbo_end(self, data):
        name = car_id(data)
        car = self.cars[name]
        car.turbo = False

    def on_turbo_available(self, data):
        self.turbo_duration_ticks = data['turboDurationTicks']
        self.turbo_factor = data['turboFactor']

        # Logic to account for case when turbo is given on game start
        if self.turbo_tick_time_last == 0:
            self.turbo_tick_time_last = -data['turboDurationTicks']

        self.ping()

    def reset_turbo(self):
        self.turbo_duration_ms = None
        self.turbo_duration_ticks = 0
        self.turbo_factor = None
        self.turbo_tick_time_last = 0
        self.turbo_on = False

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar': self.on_your_car,
            'gameStart': self.on_game_start,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions,
            'lapFinished': self.on_lap_finished,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'finish': self.on_finish,
            'turboAvailable': self.on_turbo_available,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end,
            'gameEnd': self.on_game_end,
            'tournamentEnd': self.on_tournament_end,
            'error': self.on_error,
            'dnf': self.on_disqualified,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']

            if 'gameTick' in msg:
                self.game_tick = msg['gameTick']

            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                debug_print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

    def init_track(self, track_data):
        raise NotImplementedError()

    def init_cars(self, car_data):
        raise NotImplementedError()

    def update_car_positions(self, car_position_data):
        for d in car_position_data:
            name = car_id(d['id'])
            car = self.cars[name]

            pp = d['piecePosition']

            tilt = d['angle']
            piece_num = pp['pieceIndex']
            in_piece_position = pp['inPieceDistance']
            start_lane = pp['lane']['startLaneIndex']
            end_lane = pp['lane']['endLaneIndex']
            lap = pp['lap']

            car.update_distance(in_piece_position, piece_num, start_lane, \
                    end_lane)
            car.update_tilt(tilt)

            # initialize if not initialized
            if car.current_lane is None:
                car.current_lane = start_lane 

            if start_lane != end_lane:
                car.update_lane(end_lane, piece_num)
