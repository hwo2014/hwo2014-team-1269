from csv import(
        reader
        )
from matplotlib.pyplot import(
        figure, show, plot, legend, xlabel, ylabel
        )
from numpy import(
          array, diff, nonzero, mean, exp
        , inf
        )

## Organize and plot the race data
game_tick = []
position = []
speed = []
thrust = []
slip_angle = []
curvature = []

rows = []

with open('../test.csv', 'rb') as f:
    reader = reader(f)
    for row in reader:
        rows.append(row)

rows = rows[7:-3:1]

for row in rows:
    try:
        game_tick.append(float(row[0]))
    except:
        pass
    try:
        position.append(float(row[1]))
    except:
        pass
    try:
        speed.append(float(row[2]))
    except:
        pass
    try:
        thrust.append(float(row[3]))
    except:
        pass
    try:
        slip_angle.append(float(row[4]))
    except:
        pass
    try:
        curvature.append(float(row[5]))
    except:
        pass

game_tick = array(game_tick)
position = array(position)
speed = array(speed)
thrust = array(thrust)
slip_angle = array(slip_angle)
curvature = array(curvature)

figure()
plot(game_tick, speed / max(abs(speed)))
plot(game_tick, position / max(abs(position)))
plot(game_tick, thrust / max(abs(thrust)))
plot(game_tick, slip_angle / max(abs(slip_angle)))
plot(game_tick, curvature / max(abs(curvature)))
legend(['speed', 'position', 'thrust', 'slip_angle', 'curvature'])
xlabel('Game Tick [1/60 s]')
ylabel('Normalized Parameters')

## Calculate physics coefficient

## Calculate the terminal velocity
acceleration = diff(speed)
end_rise = nonzero(acceleration == 0)[0][0]
velt = speed[end_rise]

## Calculate beta
norm_speed = speed[0:end_rise] / velt
norm_acceleration = acceleration[0:end_rise] / velt

window = 5
sample_acc = norm_acceleration[end_rise / 2 - window: end_rise / 2 + window]
sample_speed = -(norm_speed[end_rise / 2 - window: end_rise / 2 + window] - 1)
sample_thrust = mean(thrust[end_rise / 2 - window: end_rise / 2 + window])

beta = mean(sample_acc / sample_speed)

## Calculate alpha
alpha = beta * velt / sample_thrust

## Plot error in fit
vel_model = velt - velt * exp(-beta * game_tick)
error = speed - vel_model
figure()
plot(game_tick, speed)
plot(game_tick, vel_model)
xlabel('Game tick')
ylabel('Speed')
legend(['data', 'model'])
figure()
plot(game_tick, error)
ylabel('Error in speed model')
xlabel('Game tick')

## Physics parameters in dictionary
phy_params = {'alpha': alpha, 'beta': beta}
print 'Alpha: {0:.4f}\nBeta: {1:.4f}'.format(phy_params['alpha'], \
        phy_params['beta'])

## Slip physics
## Calculate centripetal acceleration
ac = speed**2 / curvature
ac[nonzero(ac == inf)] = 0
first_slip = nonzero(abs(slip_angle) > 0)[0][0]
ac_ub = abs(ac[first_slip])
ac_lb = max(abs(ac[0:first_slip - 1]))
print '{0:.4f} <= ac_thresh <= {1:.4f}'.format(ac_lb, ac_ub)

show()
