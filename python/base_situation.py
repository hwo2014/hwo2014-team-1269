
class Situation(object):
    """
    A situation abstracts various possibilities that occur during a race, and
    how to handle them.  It provides extra structure beyond simple conditionals.
    """

    def __init__(self, priority):
        self.previously_active = False

        # priority determines which situation is given control if there are
        # multiple situations occurring at once; i.e. it determines which
        # "handle" method is called
        self.priority = priority

        # convenience so we don't need to user super
        self.setup()

    def is_active(self, bot, track, cars, our_car):

        active = self.sub_is_active(bot, track, cars, our_car)
        previously_active = self.previously_active

        if active and not previously_active:
            self.on_start(bot, track, cars, our_car)

        if not active and previously_active:
            self.on_finish(bot, track, cars, our_car)

        self.previously_active = active

        return active


    def sub_is_active(self, bot, track, cars, our_car):
        """
        Given the bot instance, determine if this situation is active.
        Should return true or false.  
        Gaurunteed to run on every cycle.
        """
        raise NotImplementedError()

    def handle(self, bot, track, cars, our_cars):
        """
        Called on each iteration when this situation is active and has highest
        priority.

        Should return:
            - "Left"
            - "Right"
            - "Boost"
            - throttle  # as a float
            - None  # defer to next highest priority situation
        """
        raise NotImplementedError()

    def setup(self):
        pass

    def on_start(self, bot, track, cars, our_car):
        """Any special code that needs to be run when this situation starts."""
        pass

    def on_finish(self, bot, track, cars, our_car):
        """Run after this situation became not active."""
        pass

