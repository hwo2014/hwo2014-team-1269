from base_situation import Situation
import random


class SituationPhysics(Situation):

    def setup(self):
        self.end_tick = None

    def sub_is_active(self, bot, track, cars, our_car):
        already_run = not bot.constants is None
        enough_data = our_car.crashed or bot.game_tick == self.end_tick
        return bot.qualify_round and not already_run and not enough_data

    def handle(self, bot, track, cars, our_car):
        return 0.4

    def on_start(self, bot, track, cars, our_car):
        self.end_tick = bot.game_tick + 200

    def on_finish(self, bot, track, cars, our_car):
        distances = our_car.distances

        print(repr(distances))
        bot.constants = True  # TODO: update


class SituationDefault(Situation):

    def sub_is_active(self, bot, track, cars, our_car):
        return True

    def handle(self, bot, track, cars, our_car):
        if bot.game_tick % 100:
            return bot.governor
        else:
            return random.choice(["Left", "Right", "Boost"])
